# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-05-15 16:57+0300\n"
"PO-Revision-Date: 2018-05-23 01:34+0000\n"
"Last-Translator: Joaquín Serna <bubuanabelas@cryptolab.net>\n"
"Language-Team: Spanish <http://translate.tails.boum.org/projects/tails/"
"install-mac-usb/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Install from Mac using the command line\"]]\n"
msgstr "[[!meta title=\"Instalar desde Mac usando la línea de comandos\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr "[[!meta robots=\"noindex\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/mac-usb\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/mac-usb\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"step-image\">[[!img install/inc/infography/os-mac.png link=\"no\" alt=\"\"]]</div>\n"
msgstr "<div class=\"step-image\">[[!img install/inc/infography/os-mac.png link=\"no\" alt=\"\"]]</div>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"hidden-step-1\"></div>\n"
msgstr "<div class=\"hidden-step-1\"></div>\n"

#. type: Plain text
#, no-wrap
msgid "<p class=\"start\">Start in macOS.</p>\n"
msgstr "<p class=\"start\">Iniciar en macOS.</p>\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/install_intermediary_intro.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/install_intermediary_intro.inline.es\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Bullet: '  1. '
msgid "Make sure that the first USB stick is unplugged."
msgstr "Asegúrate de que la primera memoria USB está desenchufada."

#. type: Plain text
#, no-wrap
msgid ""
"  1. Open <span class=\"application\">Terminal</span> from\n"
"     <span class=\"menuchoice\">\n"
"       <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"       <span class=\"guisubmenu\">Utilities</span>&nbsp;▸\n"
"       <span class=\"guimenuitem\">Terminal.app</span></span>.\n"
msgstr ""
"  1. Abre <span class=\"application\">Terminal</span> desde\n"
"     <span class=\"menuchoice\">\n"
"       <span class=\"guimenu\">Aplicaciones</span>&nbsp;▸\n"
"       <span class=\"guisubmenu\">Utilidades</span>&nbsp;▸\n"
"       <span class=\"guimenuitem\">Terminal.app</span></span>.\n"

#. type: Bullet: '  1. '
msgid "Execute the following command:"
msgstr "Ejecuta el siguiente comando:"

#. type: Plain text
#, no-wrap
msgid "     <p class=\"pre command\">diskutil list</p>\n"
msgstr "     <p class=\"pre command\">diskutil list</p>\n"

#. type: Plain text
#, no-wrap
msgid "     It returns a list of the storage devices on the system. For example:\n"
msgstr "     Devuelve una lista de los dispositivos de almacenamiento en el sistema. Por ejemplo:\n"

#. type: Plain text
#, no-wrap
msgid ""
"     <p class=\"pre command-output\">/dev/disk0\n"
"      #:                     TYPE NAME         SIZE      IDENTIFIER\n"
"      0:    GUID_partition_scheme             *500.1 GB  disk0\n"
"      1:                      EFI              209.7 MB  disk0s1\n"
"      2:                Apple_HFS MacDrive     250.0 GB  disk0s2\n"
"      3:                      EFI              134.1 GB  disk0s3\n"
"      4:     Microsoft Basic Data BOOTCAMP     115.5 GB  disk0s4</p>\n"
msgstr ""
"     <p class=\"pre command-output\">/dev/disk0\n"
"      #:                     TYPE NAME         SIZE      IDENTIFIER\n"
"      0:    GUID_partition_scheme             *500.1 GB  disk0\n"
"      1:                      EFI              209.7 MB  disk0s1\n"
"      2:                Apple_HFS MacDrive     250.0 GB  disk0s2\n"
"      3:                      EFI              134.1 GB  disk0s3\n"
"      4:     Microsoft Basic Data BOOTCAMP     115.5 GB  disk0s4</p>\n"

#. type: Plain text
#, no-wrap
msgid "     <div class=\"step-image\">[[!img install/inc/infography/plug-first-usb.png link=\"no\" alt=\"\"]]</div>\n"
msgstr "     <div class=\"step-image\">[[!img install/inc/infography/plug-first-usb.png link=\"no\" alt=\"\"]]</div>\n"

#. type: Bullet: '  1. '
msgid "Plug the first USB stick in the computer."
msgstr "Conecta la primer memoria USB a la computadora."

#. type: Bullet: '  1. '
msgid "Execute again the same command:"
msgstr "Ejecuta otra vez el mismo comando:"

#. type: Plain text
#, no-wrap
msgid ""
"     Your USB stick appears as a new device in the list. Check\n"
"     that its size corresponds to the size of your USB stick.\n"
msgstr ""
"     Tu memoria USB aparece como un nuevo dispositivo en la lista. Comprueba\n"
"     que su tamaño corresponda con el tamaño de tu memoria USB.\n"

#. type: Plain text
#, no-wrap
msgid ""
"     <p class=\"pre command-output\">/dev/disk0\n"
"      #:                     TYPE NAME         SIZE      IDENTIFIER\n"
"      0:    GUID_partition_scheme             &lowast;500.1 GB  disk0\n"
"      1:                      EFI              209.7 MB  disk0s1\n"
"      2:                Apple_HFS MacDrive     250.0 GB  disk0s2\n"
"      3:                      EFI              134.1 GB  disk0s3\n"
"      4:     Microsoft Basic Data BOOTCAMP     115.5 GB  disk0s4\n"
"    /dev/disk1\n"
"      #:                     TYPE NAME         SIZE      IDENTIFIER\n"
"      0:   FDisk_partition_scheme             *8.0 GB    disk1\n"
"      1:                Apple_HFS Untitled 1   8.0 GB    disk1s1</p>\n"
msgstr ""
"     <p class=\"pre command-output\">/dev/disk0\n"
"      #:                     TYPE NAME         SIZE      IDENTIFIER\n"
"      0:    GUID_partition_scheme             &lowast;500.1 GB  disk0\n"
"      1:                      EFI              209.7 MB  disk0s1\n"
"      2:                Apple_HFS MacDrive     250.0 GB  disk0s2\n"
"      3:                      EFI              134.1 GB  disk0s3\n"
"      4:     Microsoft Basic Data BOOTCAMP     115.5 GB  disk0s4\n"
"    /dev/disk1\n"
"      #:                     TYPE NAME         SIZE      IDENTIFIER\n"
"      0:   FDisk_partition_scheme             *8.0 GB    disk1\n"
"      1:                Apple_HFS Untitled 1   8.0 GB    disk1s1</p>\n"

#. type: Bullet: '  1. '
msgid ""
"Take note of the *device name* of your USB stick.  In this example, the USB "
"stick is 8.0 GB and its device name is <span class=\"code\">/dev/disk1</"
"span>.  Yours might be different."
msgstr ""
"Toma nota del *nombre de dispositivo* de tu memoria USB.  En este ejemplo, "
"la memoria USB es de 8.0 GB y su nombre de dispositivo es <span class=\"code"
"\">/dev/disk1</span>.  El tuyo puede ser diferente."

#. type: Plain text
#, no-wrap
msgid ""
"     <div class=\"caution\">\n"
"     <p>If you are unsure about the device name, you should stop proceeding or\n"
"     <strong>you risk overwriting any hard disk on the system</strong>.</p>\n"
"     </div>\n"
msgstr ""
"     <div class=\"caution\">\n"
"     <p>Si no estás seguro del nombre del dispositivo deberías parar ahora o\n"
"     <strong>te arriesgas a sobreescribir cualquier otro disco duro del sistema</strong>.</p>\n"
"     </div>\n"

#. type: Plain text
#, no-wrap
msgid "     <div class=\"step-image\">[[!img install/inc/infography/install-intermediary-tails.png link=\"no\" alt=\"\"]]</div>\n"
msgstr "     <div class=\"step-image\">[[!img install/inc/infography/install-intermediary-tails.png link=\"no\" alt=\"\"]]</div>\n"

#. type: Plain text
#, no-wrap
msgid ""
"  1. Execute the following commands to copy the ISO image that you\n"
"          downloaded earlier to the USB stick.\n"
msgstr ""
"  1. Ejecuta los siguientes comandos para copiar la imagen ISO que\n"
"          descargaste antes a la memoria USB.\n"

#. type: Plain text
#, no-wrap
msgid "     Replace:\n"
msgstr "     Replace:\n"

#. type: Bullet: '     - '
msgid ""
"<span class=\"command-placeholder\">tails.iso</span> with the path to the "
"ISO image"
msgstr ""
"<span class=\"command-placeholder\">tails.iso</span> con la ruta de la "
"imagen ISO"

#. type: Plain text
#, no-wrap
msgid ""
"       <div class=\"tip\">\n"
"       <p>If you are unsure about the path to the ISO image, you can insert the\n"
"       correct path by dragging and dropping the icon of the ISO image from\n"
"       <span class=\"application\">Finder</span> onto <span class=\"application\">\n"
"       Terminal</span>.</p>\n"
"       </div>\n"
msgstr ""
"       <div class=\"tip\">\n"
"       <p>Si estás inseguro de la ruta de la imagen ISO, puedes insertar la\n"
"       ruta correcta al arrastrar el ícono de la imagen ISO desde el\n"
"       <span class=\"application\">Navegador de Archivos</span> hasta la <span class=\"application\">\n"
"       Terminal</span>.</p>\n"
"       </div>\n"

#. type: Bullet: '     - '
msgid ""
"<span class=\"command-placeholder\">device</span> with the device name found "
"in step 6"
msgstr ""
"<span class=\"command-placeholder\">device</span> con el nombre del "
"dispositivo encontrado en el paso 6"

#. type: Plain text
#, no-wrap
msgid ""
"       <div class=\"tip\">\n"
"       <p>You can try adding <span class=\"code\">r</span> before <span class=\"code\">disk</span> to make the installation faster.</p>\n"
"       </div>\n"
msgstr ""
"       <div class=\"tip\">\n"
"       <p>Puedes probar agregando <span class=\"code\">r</span> antes de <span class=\"code\">disco</span> para acelerar la instalación.</p>\n"
"       </div>\n"

#. type: Plain text
#, no-wrap
msgid "     <p class=\"pre command\">diskutil unmountDisk <span class=\"command-placeholder\">device</span></p>\n"
msgstr ""
"     <p class=\"pre command\">diskutil unmountDisk <span class=\"command-"
"placeholder\">device</span></p>\n"

#. type: Plain text
#, no-wrap
msgid "     <p class=\"pre command\">dd if=<span class=\"command-placeholder\">tails.iso</span> of=<span class=\"command-placeholder\">device</span> bs=16m && sync</p>\n"
msgstr ""
"     <p class=\"pre command\">dd if=<span class=\"command-placeholder\""
">tails.iso</span> of=<span class=\"command-placeholder\">device</span> bs="
"16m && sync</p>\n"

#. type: Plain text
#, no-wrap
msgid "     You should get something like this:\n"
msgstr "     Deberías recibir algo como esto:\n"

#. type: Plain text
#, no-wrap
msgid "     <p class=\"pre command-example\">dd if=/Users/me/tails-amd64-3.0.iso of=/dev/rdisk9 bs=16m && sync</p>\n"
msgstr ""
"     <p class=\"pre command-example\">dd if=/Users/me/tails-amd64-3.0.iso "
"of=/dev/rdisk9 bs=16m && sync</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"     If no error message is returned, Tails is being copied on the USB\n"
"     stick. The copy takes some time, generally a few minutes.\n"
msgstr ""
"     Si no aparece ningún mensaje de error, Tails está siendo copiado a la memoria USB.\n"
"     El proceso de copiado tarda por lo general un par de minutos.\n"

#. type: Plain text
#, no-wrap
msgid ""
"     <div class=\"note\">\n"
"     <p>If you get a <span class=\"guilabel\">Permission denied</span> error, try\n"
"     adding <code>sudo</code> at the beginning of the command:</p>\n"
msgstr ""
"     <div class=\"note\">\n"
"     <p>Si recibes un error de <span class=\"guilabel\">Permiso denegado</span>, prueba\n"
"     agregando <code>sudo</code> al principio del comando:</p>\n"

#. type: Plain text
#, no-wrap
msgid "     <p class=\"pre command\">sudo dd if=<span class=\"command-placeholder\">tails.iso</span> of=<span class=\"command-placeholder\">device</span> bs=16m && sync</p>\n"
msgstr ""
"     <p class=\"pre command\">sudo dd if=<span class=\"command-placeholder\""
">tails.iso</span> of=<span class=\"command-placeholder\">device</span> bs="
"16m && sync</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"     <p>If you get an <span class=\"guilabel\">invalid number ‘16m’</span> error, try\n"
"     using <code>16M</code> instead:</p>\n"
msgstr ""
"     <p>Si recibes un error <span class=\"guilabel\">número inválido ‘16m’</span>, prueba\n"
"     utilizar <code>16M</code>:</p>\n"

#. type: Plain text
#, no-wrap
msgid "     <p class=\"pre command\">dd if=<span class=\"command-placeholder\">tails.iso</span> of=<span class=\"command-placeholder\">device</span> bs=16M && sync</p>\n"
msgstr ""
"     <p class=\"pre command\">dd if=<span class=\"command-placeholder\""
">tails.iso</span> of=<span class=\"command-placeholder\">device</span> bs="
"16M && sync</p>\n"

#. type: Plain text
#, no-wrap
msgid "     </div>\n"
msgstr "     </div>\n"

#. type: Plain text
#, no-wrap
msgid "     The installation is complete once the command prompt reappeared.\n"
msgstr "     La instalación se ha completado una vez que reaparezca el interprete de comandos.\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/install_intermediary_outro.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/install_intermediary_outro.inline.es\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/restart_first_time.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/restart_first_time.inline.es\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/install_final.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/install_final.inline.es\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/restart_second_time.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/restart_second_time.inline.es\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/create_persistence.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/create_persistence.inline.es\" raw=\"yes\" sort=\"age\"]]\n"

#~ msgid "     <pre>diskutil unmountDisk [device]</pre>\n"
#~ msgstr "     <pre>diskutil unmountDisk [dispositivo]</pre>\n"

#~ msgid "     <pre>dd if=[tails.iso] of=[device] bs=16m && sync</pre>\n"
#~ msgstr "     <pre>dd if=[tails.iso] of=[dispositivo] bs=16m && sync</pre>\n"

#~ msgid "     <pre>sudo dd if=[tails.iso] of=[device] bs=16m && sync</pre>\n"
#~ msgstr "     <pre>sudo dd if=[tails.iso] of=[dispositivo] bs=16m && sync</pre>\n"

#~ msgid "     <pre>dd if=[tails.iso] of=[device] bs=16M && sync</pre>\n"
#~ msgstr "     <pre>dd if=[tails.iso] of=[dispositivo] bs=16M && sync</pre>\n"

#~ msgid ""
#~ "[[!meta stylesheet=\"inc/stylesheets/dave\" rel=\"stylesheet\" title="
#~ "\"\"]]\n"
#~ msgstr ""
#~ "[[!meta stylesheet=\"inc/stylesheets/dave\" rel=\"stylesheet\" title="
#~ "\"\"]]\n"

#~ msgid ""
#~ "[[!inline pages=\"install/inc/tails-installation-assistant.inline\" raw="
#~ "\"yes\" sort=\"age\"]]\n"
#~ msgstr ""
#~ "[[!inline pages=\"install/inc/tails-installation-assistant.inline.es\" "
#~ "raw=\"yes\" sort=\"age\"]]\n"

#~ msgid ""
#~ "[[!inline pages=\"install/inc/steps/download.inline\" raw=\"yes\" sort="
#~ "\"age\"]]\n"
#~ msgstr ""
#~ "[[!inline pages=\"install/inc/steps/download.inline.es\" raw=\"yes\" sort="
#~ "\"age\"]]\n"

#~ msgid ""
#~ "Execute the following command to safely remove the USB stick. Replace "
#~ "<span class=\"code\">[device]</span> with the device name found in step 5."
#~ msgstr ""
#~ "Ejecuta el siguiente comando para expulsar con seguridad la memoria USB. "
#~ "Reemplaza <span class=\"code\">[dispositivo]</span> con el nombre del "
#~ "dispositivo encontrado en el paso 5."
